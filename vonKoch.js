/*
 * Programme permettant de représenter un flocon de Von Koch.
 * Il s'agit d'une fractale qui s'obtient en dessinant un triangle, puis
 * en enlevant le le 2ème tiers de chaque segment du triangle, pour y placer
 * un triangle 3 fois plus petit, puis on l'on recommence avec chaque segment
 * de la forme ainsi obtenue...
 */

// ---- VARIABLES GLOBALES ---- //

var slider;	// Slider pour choisir les itérations.

// ---- ---- //

/*
 * Fonction de setup de p5.js.
 */
function setup() {
	/* Fonction permettant de redessiner l'écran.
	* La fonction draw() de p5.js n'est pas utilisée ici, car le calcul de la fractale dans celle-çi
	* ralentirait considérablement le navigateur.
	* 7 itérations mettent entre 0.25 et 0.27 s à être calculées, ce qui signifie que l'on aurait à 
	* 7 itérations moins de 4 fps si la fractale était calculée dans draw().
	* On préfère donc appeller redraw() à chaque mise à jour du curseur.
	*/
	function redraw() {
		clear();
		textSize(30);
		text('Flocon de Von Koch', 1, 30);
		textSize(15);
		var iterations = slider.value();
		text('Itérations : ' + iterations, 1, 62);
		vonKoch(400, 400, 675, iterations);
	}
	// Création canvas.
	createCanvas(1000, 1000);
	// Setup texte statique
	// Setup slider.
	slider = createSlider(0, 7, 1, 1);
	slider.position(80, 52.5);
	slider.size(180, 10);
	// Rafraîchissement écran.
	redraw();
	slider.changed(redraw);	// Callback de redraw() à chaque mise à jour du slider.
}

/*
 * Fonction de dessin de la fractale.
 * Paramètres :
 * 	- xCenter : abscisse du milieu de la fractale.
 *  - yCenter : ordonnée du milieu de la fractale.
 *  - size : côté du triangle initial.
 *  - iterations : nombre d'itérations.
 */
function vonKoch(xCenter, yCenter, size, iterations) {
	function getSide(size, n) {	// Fonction de calcul de côté en fonction de l'itération actuelle.
		return size * Math.pow(1 / 3, n);
	}
	var centers = [];	// Milieux des triangles à la précédente itération.
	for (var i = 0; i < iterations; i++) {
		// Calcul des milieux des triangles
		var side = getSide(size, i);	// Côté à l'itération actuelle.
		var currentCenters = [];	// Milieux de l'itération actuelle.
		if (i === 0)
			// [[[abscisseMilieu, ordonnéeMilieu], estInversé]]
			currentCenters = [[[xCenter, yCenter], false]];	// Première itération : on définit le premier milieu.
		else  {
			for (var i1 = 0; i1 < centers.length ; i1++) {
				var center = centers[i1];
				var coords = center[0];
				var centerTPoints = getTrianglePoints(
					coords[0], coords[1], 2 / 3 * getSide(size, i - 1), !center[1]);	// On calcule les milieux des nouveaux triangles.
				for (var i2 = 0; i2 < centerTPoints.length; i2++) {
					currentCenters.push([centerTPoints[i2], !center[1]]);	// Ajout à currentCenters.
				}	
			}
		}
		// Affichage de la forme.
		for (var i1 = 0; i1 < currentCenters.length; i1++) {
			// On dessine les triangles 1 à 1.
			var center = currentCenters[i1];
			drawEqTriangle(center[0][0], center[0][1], side, center[1]);
		}
		// Préparation prochaine itération.
		if (i > 0) {
			// Certains triangles ne peuvent pas être dessinés à partir des milieux détérminés precédemeent.
			// Il faut donc rajouter des milieux pour la prochaine itération.
			for (var i1 = 0; i1 < centers.length; i1++) {
				var center = centers[i1];
				var coords = center[0];
				var currentCenterTPoints = getTrianglePoints(
					coords[0], coords[1], 2 / 3 * getSide(size, i-1), center[1]);
				for (var i2 = 0; i2 < currentCenterTPoints.length; i2++) {
					currentCenters.push([currentCenterTPoints[i2], center[1]]);
				}
			}
		}
		centers = currentCenters;	// On met à jour centers pour la prochaine itération.
	}
}

/*
 * Fonction de dessin d'un triangle équilatéral.
 * Paramètres :
 * 	- xCenter : abscisse du milieu du triangle.
 *  - yCenter : ordonnée du milieu du triangle.
 *  - side : longueur du côté du triangle.
 *  - inverted : true si inversé (sommet vers le bas), false sinon.
 */
function drawEqTriangle(xCenter, yCenter, side, inverted) {
	var tPoints = getTrianglePoints(xCenter, yCenter, side, inverted);	// Calcul points
	fill(0);	// Couleur remplissage noire
	triangle(	// Dessin triangle.
		tPoints[0][0], tPoints[0][1],
		tPoints[1][0], tPoints[1][1],
		tPoints[2][0], tPoints[2][1]
	);
}

/*
 * Détermine les cordonnées des sommets d'un triangle équilatéral.
 * Paramètres :
 * 	- xCenter : abscisse du milieu du triangle.
 *  - yCenter : ordonnée du milieu du triangle.
 *  - side : longueur du côté du triangle.
 *  - inverted : true si inversé (sommet vers le bas), false sinon.
 *
 * Renvoie une array contenant les coordonnées des sommets A, B et C :
 * [[xA, yA], [xB, yB], [xC, yC]]
 */
function getTrianglePoints(xCenter, yCenter, side, inverted) {
	// Côté
	side = (inverted) ? -Math.abs(side) : Math.abs(side);	// Changements de signe si inverted vrai pour inverser le triangle.
	// Hauteur triangle.
	var tHeight = (inverted) ? -getEqTriangleHeight(side) : getEqTriangleHeight(side);
	var yAB = yCenter + 1 / 3 * tHeight;	// On détérmine l'ordonnée commune à deux points du triangle.
	// On retourne l'array contenant les coordonnées.
	return [
		[xCenter - 1 / 2 * side, yAB],
		[xCenter + 1 / 2 * side, yAB],
		[xCenter, yCenter  - 2 / 3 * tHeight]
	];
}

/*
 * Renvoie la hauteur d'un triangle équilatéral en fonction
 * de la longueur de ses côtés.
 * Paramètres :
 *	- side : longueur du côté du triangle.
 */
function getEqTriangleHeight(side) {
	return Math.sqrt(3 / 4 * Math.pow(side, 2));
}
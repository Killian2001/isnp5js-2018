// ---- VARIABLES GLOBALES ---- //

/* Constantes */
/* Il est possible de modifier ces constantes afin de modifier le comportement des vagues. */
// Canvas
const CANVAS_WIDTH = 800;	// Longueur Canvas (en px).
const CANVAS_HEIGHT = 800;	// Hauteur Canvas (en px).

// Vagues
const WAVES_LENGTH = 800;	// Longueur vagues (en px).
const WAVES_SPEED = 125;		// Vitesse (en px/s).
const WAVES_AMPLITUDE = 50;	// Amplitude des vagues (en px).
const WAVES_MIN_HEIGHT = 250;	// Hauteur minimum des vagues (en px)
const WAVES_MAX_HEIGHT = 450;	// Hauteur maximum des vagues (en px)
const WAVES_HEIGHT_VARIATION_PERIOD = 10;	// Période de variation de la hauteur des vagues (en s).

// Oiseaux
const BIRDS_FLAP_FREQ = 1;	// Fréquence de battement des ailes des oiseaux (en Hz).
const BIRDS_MIN_SPEED = 20;	// Vitesse minimum de déplacement des oiseaux (en px/s).
const BIRDS_MAX_SPEED = 70;		// Vitesse maximum de déplacement des oiseaux (en px/s).

/* Variables */

var wavesShift = 0;	// Décalage des vagues.
var prevTime;	// Dernier temps enregistré (en ms)
var birds = [];	// Array oiseaux

// ---- ---- //

/*
 * Fonction setup de p5.js
 */
function setup() {
	createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);	// Création du canvas.
	background(255);	// Couleur d'arrière plan du canvas (ici blanc).
	frameRate(60);	// 60 fps
	prevTime = millis(); // On initialise prevTime
}

/*
 * Fonction de dessin de p5.js.
 */
function draw() {
	clear();	// Effacage de l'écran.
	var birdRandom = Math.floor(random(1000));	// Nombre aléatoire génération oiseaux.
	if (birdRandom === 1 && birds.length < 10)	// Si nombre aléatoire = 1 (1 chance sur 1000). Limite de 10 oiseaux dans l'array.
		birds.push(new Bird(	// Création nouvel oiseau.
			-10, // Abscisse d'apparition (en dehors de l'écran).
			random(0, WAVES_MIN_HEIGHT - WAVES_AMPLITUDE), // Hauteur aléatoire, au dessus des vagues.
			random(BIRDS_MIN_SPEED, BIRDS_MAX_SPEED), // Vitesse aléatoire.
			BIRDS_FLAP_FREQ));
	for (var i = 0; i < birds.length; i++) {	// Affichage des oiseaux. 
		var b = birds[i];
		if (b.x > CANVAS_WIDTH + 10)	// Si oiseau en dehors de l'écran, suppression de l'oiseau de l'array.
			birds.splice(i, 1);
		b.x += b.speed * (millis() - prevTime) / 1000;	// On déplace l'oiseau en fonction de sa vitesse.
		paintBird(b);	// On affiche l'oiseau.
	}
	stroke(0, 0, 255);	// Contours des formes en bleu.
	fill(0, 0, 255);	// Remplissage des formes en bleu.
	// On incrémente le décalage des vagues avec la vitesse des vagues, en prenant en compte le temps écoulé entre le dernier dessin.
	wavesShift += WAVES_SPEED * (millis() - prevTime) / 1000;
	beginShape();	// On commence le dessin des vagues.
	wavesHeight = 
			rangeSin(millis(), WAVES_MIN_HEIGHT, WAVES_MAX_HEIGHT, WAVES_HEIGHT_VARIATION_PERIOD * 1000);	// On détermine la hauteur des vagues.
	for(var x = 1; x <= CANVAS_WIDTH; x++) {
		vertex(	// Ajout d'un point au contour des vagues.
			x, 
			rangeSin((x - wavesShift), -WAVES_AMPLITUDE, WAVES_AMPLITUDE, WAVES_LENGTH) + wavesHeight
		);
	}
	// On ferme la forme correspondant au vagues.
	vertex(CANVAS_WIDTH, CANVAS_HEIGHT);
	vertex(1, CANVAS_HEIGHT);
	endShape();	// Fin du tracé.
	prevTime = millis();	// Mise à jour dernier temps enregistré.
}

/*
 * Fonction sinus dont les valeurs seront comprises sur un intervalle fermé.
 * Paramètres :
 *  - x : antécédent.
 *  - min : valeur minimum de fonction.
 *  - max : valeur maximum de la fonction.
 * Exceptions :
 *  - RangeError : si le minimum est supérieur au maximum.
 */
function rangeSin(x, min, max, period) {
	if (min > max)	// Vérif valeur min max.
		throw new RangeError('Le minimum doit être inférieur au maximum.');
	return (max - min) / 2 * Math.sin((2 * Math.PI) / period * x) + (min + max) / 2;
}

/*
 * Classe représentant un oiseau affiché à l'écran.
 * Un oiseau est caractérisé par sa position (abscisse; ordonnée),
 * par sa vitesse et la fréquence du cycle de battement de ses ailes.
 */
class Bird {
	/*
	 * Constructeur de la classe.
	 * Paramètres :
	 * 	- x : abscisse de l'oiseau.
	 * 	- y : ordonnée de l'oiseau.
	 * 	- speed : vitesse de l'oiseau.
	 * 	- flapFreq : fréquence du cycle de battement de l'oiseau.
	 */
	constructor(x, y, speed, flapFreq) {
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.flapFreq = flapFreq;
	}
	
	/* ---- Accesseurs ---- */
	
	/*
	 * Accesseur de la propriété x.
	 * Renvoie l'abscisse de l'oiseau.
	 */
	get x() {
		return this.m_x;	
	}
	
	/*
	 * Accesseur de la propriété y.
	 * Renvoie l'ordonnée de l'oiseau.
	 */
	get y() {
		return this.m_y;
	}
	
	/*
	 * Accesseur de la propriété speed.
	 * Renvoie la vitesse de l'oiseau.
	 */
	get speed() {
		return this.m_speed;
	}
	
	/*
	 * Accesseur de la propriété flapFreq.
	 * Renvoie la fréquence du cycle de battement des ailes de l'oiseau.
	 */
	get flapFreq() {
		return this.m_flapFreq;	
	}
	
	/* ---- Mutateurs ---- */
	
	/*
	 * Mutateur de la propriété x.
	 * Modifie l'abscisse de l'oiseau.
	 * Paramètres :
	 *	- x : nouvelle abscisse de l'oiseau.
	 */
	set x(x) {
		this.m_x = x;	
	}
	
	/*
	 * Mutateur de la propriété y.
	 * Modifie l'ordonnée de l'oiseau.
	 * Paramètres :
	 *	- x : nouvelle ordonnée de l'oiseau.
	 */
	set y(y) {
		this.m_y = y;	
	}
	
	/*
	 * Mutateur de la propriété speed.
	 * Modifie la vitesse de l'oiseau.
	 * Paramètres :
	 *	- x : nouvelle vitesse de l'oiseau.
	 */
	set speed(speed) {
		this.m_speed = speed;	
	}
	
	/*
	 * Mutateur de la propriété flapFreq.
	 * Modifie la fréquence du cycle de battement des ailes de l'oiseau.
	 * Paramètres :
	 *	- x : nouvelle fréquence du cycle de battement des ailes de l'oiseau.
	 */
	set flapFreq(flapFreq) {
		this.m_flapFreq = Math.abs(flapFreq);
	}
}

/*
 * Fonction de dessin d'un oiseau.
 * Paramètres :
 *	- bird : oiseau à dessiner.
 */
function paintBird(bird) {
	stroke(0);	// Ligne noire.
	var millisBirdsFlapPeriod = Math.round(1 / bird.flapFreq * 1000);	// On détérmine la période à partir de la fréquence de battement des ailes.
	var lineY = (millis() % millisBirdsFlapPeriod < millisBirdsFlapPeriod / 2) ? bird.y - 10 : bird.y + 10;	// On détérmine une ordonnée, le sens des ailes en dépendera.
	// Dessin oiseau.
	line(bird.x, bird.y, bird.x + 10, lineY);
	line(bird.x, bird.y, bird.x - 10, lineY);
}
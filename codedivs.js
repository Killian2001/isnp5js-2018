/*
 * Programme de gestion de l'affichage de codes sources en HTML.
 * Affiche des programmes en programation textuelle et visuelle.
 * Utilise la bibliothèque highlight.js pour la coloration syntaxique des programmes textuel.
 *
 * NOTE : les fonction de génération du HTML et création d'éléments ne sont nécéssaire que pour que
 * cela fonctionne sur OpenProcessing.
 *
 * Utilise pour la coloration syntaxique la bibliothèque highlight.js. 
 *
 * Mentions légales pour highlight.js (licence BSD) :
 * Copyright (c) 2006, Ivan Sagalaev
 * All rights reserved.
 * (http://git.io/hljslicense)
 */

/*
 * Fonction de setup de p5.
 */
function setup() {
	// Création d'un canvas de 800x800 pour redimensionner l'iframe dans lequel sera affiché la page.
	var canvas = createCanvas(1000, 1000);
	canvas.hide();	// On cache le canvas (gêne au niveau des interactions avec la page sinon).
	generateHTML();	// Génération du HTML
}

/*
 * [NECESSAIRE SUR OPENPROCESSING]
 * Génère le HTML de la page.
 */
function generateHTML() {
	var cssLink = createElement('link');	// Ajout d'un élément <link> pour le CSS pour highlight.js.
	// On définit les propriétés de l'élément <link>.
	cssLink.attribute('rel', 'stylesheet');	// Rôle de l'élément <link>
	cssLink.attribute('href', 'https://thimbleprojects.org/killian2001/561182/styles/monokai-sublime.css');	// Feuille de style highlight.js (depuis thimble).
	cssLink.parent(select('head'));	// Ajout de l'élément <link> dans l'élément <head> de la page.
	// Création des éléments de code.
	var pc = createPreElement('Pseudocode', 'nohighlight', 'Si Vrai alors\n\tAfficher &laquo;Hello World !&raquo;\nFinSi');	// Pseudocode.
	var py = createPreElement('Python', 'python', "if True:\n    print('Hello World !')");	// Python
	var js = createPreElement('Javascript', 'javascript', "if (true)\n    alert('Hello World !');");	// Javascript
	var swift = createPreElement('Swift', 'swift', "if true {\n    print(\"Hello World !\")\n}");	// Swift
	var c = createPreElement('C', 'c', "#include &lt;stdio.h&gt;\n\n" +
													 "int main(int argc, char **argv)\n" +
													 "{\n" +
													 "    if (true)\n" +
													 "        printf(\"Hello World !\\n\");\n" +
													 "    return 0;\n" +
													 
													 "}\n");	// C
	var alg = createImgElement('Algorigramme', "prog.svg", 'Algorigramme du programme');	// Algorigramme (graphique, SVG)
	var scratch = createImgElement('Scratch', 'prog.PNG', 'Programme Scratch');	// Scratch (graphique, PNG).
	var div = createDiv();	// Création de la div.
	// Style de la div
	div.style('margin', '1em');	// Marges extérieures
	div.style('padding', '1em');	// Marges intérieures
	div.style('border', '1px black solid');	// Bordure de la div (1 pixel, noir, trait continu)
	var buttonDiv = createDiv();	// Création de la div pour les boutons.
	buttonDiv.class('buttonDiv');	// Identification de la div pour les boutons avec une classe.
	buttonDiv.parent(div);	// Ajout de la div bouton dans la div principale.
	// Ajout des codes dans la div.
	pc.parent(div);
	py.parent(div);
	js.parent(div);
	alg.parent(div);
	scratch.parent(div);
	swift.parent(div);
	c.parent(div);
	// Traitement de la div via la classe CodeDiv.
	new CodeDiv(div, 'Pseudocode');
	// HTML Code source de la div.
	createP("Architecture de la div d'affichage des codes : ");	// Paragraphe.
	var htmlPre = createElement('pre', '&lt;div&gt;\n' + 
																'\t&lt;div class="buttonDiv"&gt;&lt;/div&gt;\t&lt;!-- Obligatoire, div qui contiendra les boutons. --&gt;\n\n' +
																'\t&lt;!-- Exemples de programmation textuelle. --&gt;\n' +
																'\t&lt;pre class="Pseudocode"&gt;&lt;code class="nohighlight"&gt;...&lt;/code&gt;&lt;/pre&gt;\t&lt;!-- Pseudocode --&gt;\n'+  
																'\t&lt;pre class="Python"&gt;&lt;code class="python"&gt;...&lt;/code&gt;&lt;/pre&gt;\t&lt;!-- Python --&gt;\n'+ 
																'\t&lt;pre class="Javascript"&gt;&lt;code class="javascript"&gt;...&lt;/code&gt;&lt;/pre&gt;\t&lt;!-- Javascript --&gt;\n'+ 
																'\t&lt;pre class="Swift"&gt;&lt;code class="swift"&gt;...&lt;/code&gt;&lt;/pre&gt;\t&lt;!-- Swift --&gt;\n'+ 
																'\t&lt;pre class="C"&gt;&lt;code class="c"&gt;...&lt;/code&gt;&lt;/pre&gt;\t&lt;!-- C --&gt;\n\n'+ 
																'\t&lt;!-- Exemple de programmation graphique --&gt;\n' +
																'\t&lt;img class="Algorigramme" src="prog.svg" alt="Algorigramme du programme" /&gt;\t&lt;!-- Algorigramme --&gt;\n'	+
																'\t&lt;img class="Scratch" src="prog.PNG" alt="Programme Scratch" /&gt;\t&lt;!-- Scratch --&gt;\n'	+
																'&lt;/div&gt;'
															);	// Balise <pre> code source de la div.
	htmlPre.style('text-align', 'justify');	// Style de la balise <pre> : justification du texte.
	htmlPre.class('html');	// Classe html pour highlight.js
	hljs.highlightBlock(htmlPre.elt);	// Coloration syntaxique.
}

/*
 * [NECESSAIRE SUR OPENPROCESSING]
 * Fonction de création d'un élément de programmation textuelle.
 * Paramètres :
 *  - preClass : classe de l'élément <pre> (nom de la classe affiché
 *  sur le bouton correspondant au langage).
 *  - hljsClass : classe de l'élément <code> (langage pour highlight.js).
 *  - content : code contenu dans l'élément <code>.
 * Retourne l'élément créé.
 */
function createPreElement(preClass, hljsClass, content) {
	var pre = createElement('pre');	// Création élément <pre>.
	var code = createElement('code', content);	// Création élément <code> (nécessaire pour highlight.js). 
	// Ajout des classes.
	pre.class(preClass);
	code.class(hljsClass);
	code.parent(pre);
	// Application styles.
	pre.style('margin', '1em auto 1em auto');	// Marges extérieures (pour centrer)
	pre.style('text-align', 'justify');	// Justification du texte.
	hljs.highlightBlock(code.elt);	// Coloration syntaxique du code.
	return pre;
}

/*
 * [NECESSAIRE SUR OPENPROCESSING]
 * Crée un élément de programmation graphique.
 * Paramètres :
 *  - language : langage utilisé (affiché sur le bouton correspondant).
 *  - href : adresse URL de l'image.
 *  - alt : texte alternatif de l'image.
 * Retourne l'élément créé.
 */
function createImgElement(language, href, alt) {
	var img = createImg(href, alt);	// Création élément <img>
	img.class(language);	// Ajout classe langage.
	// Application style.
	img.style('width', '50%');
	img.style('margin', '1px 25% 1px 25%');	// Marges extérieures (pour centrer).
	return img;
}

/* -------------------- CONTENU DE codedivs.js (FICHIER A AJOUTER DANS LA PAGE AVEC UNE BALISE <script> -------------------- */

/* 
 * codedivs.js
 * Fichier de script contenant des outils permettant de gérer l'affichage des
 * différents codes sources sur la page web.
 *
 * Utilise les bibliothèques p5.js et p5.dom.js.
 *
 */

/*
 * Classe CodeDiv
 *
 * Permet la gestion de l'affichage de plusieurs codes sources contenus dans un élément div.
 * Les changements de code se font grâce à des boutons.
 * Peut afficher des images matricielles ou vectorielles avec des éléments img.
 */
class CodeDiv {
	/*
	 * Constructeur de la classe.
	 * Paramètres :
	 *  - div (p5.Element) : div dont l'affichage des codes sources sera pris en charge.
	 *    Elle devra avoir la structure suivante :
	 *    <div>
	 *        <pre class="Langage1"><code class="langageHljs1>...</code></pre>	<!-- Exemple de programmation textuelle. -->
	 *        <img class="Langage2" ... />	<!-- Exemple de programmation graphique -->
	 *		  <div class="buttonDiv"></div>	<!-- Obligatoire, div qui contiendra les boutons -->.
	 *    </div>
	 *	- currentScript (string) : nom du script affiché par défaut.
	 * Exceptions :
	 *  - TypeError : si div n'est pas une instance de p5.Element ou si currentScript n'est pas de type string.
	 *  - Error : si div ne contient pas d'élément de classe buttonDiv, si un élément de code ne comporte pas
	 *    de classe, ou si currentScript n'est pas dans les langages disponibles (exception lancée depuis setScript).
	 */
	constructor(div, currentScript) {
		if (!div instanceof p5.Element)	// Vérification instance (si div est un objet élément html p5 ou non).
			throw new TypeError("div doit être une instance d'objet p5.Element.");	// Erreur type div
		if (!typeof(currentScript) === 'string')	// Vérif type currentScript
			throw new TypeError('currentScript doit être de type string');	// Erreur type currentScript
			
		this.div = div;	// Initialisation de la propriété div de la classe.
		
		var buttonDiv = select('.buttonDiv', this.div);	// On séléctionne la div des boutons.
		if (buttonDiv === null)	// Vérification de l'existence de la div des boutons.
			throw new Error("div ne contient pas d'élément de classe 'buttonDiv'.");	// Erreur div boutons null.
			
		var scripts = selectAll('pre', this.div).concat(selectAll('img', this.div));	// On stocke tous les éléments pre et img de la div dans une array.
		for (var i = 0; i < scripts.length; i++) {
			var scriptElt = scripts[i];	// Script à ajouter.
			var eltClass = scriptElt.class();	// On récupère la classe.
			
			if (eltClass === '')	// On vérifie l'objet possède une classe.
				throw new Error('Un élément de code (balise <pre>, <img>) ne comporte pas de classe.');	// Erreur absence de classe.
				
			// Création d'un bouton.
			let b = createButton(eltClass);
			// On associe comme action au bouton d'afficher le script qui lui correspond.
			b.mouseReleased(() => this.setScript(b.html()));
			b.parent(buttonDiv);	// On ajoute le bouton à la div.
		}
		this.setScript(currentScript);	// On affiche le script par défaut.
	}
	
	/*
	 * Change le script affiché à l'écran, en affichant celui demandé et en cachant les autres.
	 * Paramètres :
	 *  - languageName (string) : nom du langage à afficher.
	 * Exceptions : 
	 *	- TypeError : si languageName n'est pas de type string.
	 *  - Error : si languageName n'est pas dans les langages disponibles
	 */
	setScript(languageName) {
		if (typeof(languageName) !== 'string')	// Vérif type languageName
			throw new TypeError('languageName doit être de type string.');	// Erreur type languageName
		var eltToShow = select('.' + languageName, this.div);	// Elément à afficher.
		if (eltToShow === null)	// Vérification existence élément : s'il n'existe pas, on considère que la language n'est pas disponible.
			throw new Error("Le langage demandé (" + languageName + ") n'est pas présent parmis les langages disponibles");	// Erreur langage inexistant.
		if (this.currentScript !== undefined) {	// currentScript undefined <=> affichage script par défaut à l'initialisation 
			select('.' + this.currentScript, this.div).hide();	// On cache le script qui est affiché.
		} else {
			var scripts = selectAll('pre', this.div).concat(selectAll('img', this.div));	// Premier affichage : on cache tous les scripts.
			for (var i = 0; i < scripts.length; i++) {
				scripts[i].hide();
			}
		}
		this.currentScript = languageName;	// On modifie la propriété de la classe currentScript.
		eltToShow.show();
	}
}
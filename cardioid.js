/*
 * Programme permettant le tracé d'une cardioïde, une figure en forme de coeur.
 * 
 * Pour tracer une cardioïde, il faut d'abord tracer un cercle initial C, sur lequel on va
 * placer un point initial M.
 * On place ensuite des points à intervalles réguliers sur le cercle, que l'on notera P_1, P_2, P_3 ... P_x
 * Puis, pour tout n de 1 à x, on trace des cercles ayant pour centre P_n et pour rayon la distance P_nM.
 * On obtient au terme du processus une cardioïde.
 */

/* CONSTANTES */

const CANVAS_WIDTH = 1000;	// Largeur canvas (en px).
const CANVAS_HEIGHT = 1000;	// Hauteur canvas (en px).

const CARDIOID_INITIAL_CIRCLE_RADIUS = 150;	// Rayon cercle initial (en px).
const CARDIOID_INITIAL_CIRCLE_CENTER_X = 500;	// Abscisse centre cercle initial (en px).
const CARDIOID_INITIAL_CIRCLE_CENTER_Y = 500;	// Ordonnée centre cercle initial (en px).
const CARDIOID_ORIENTATION = -(1/2) * Math.PI;	// Orientation de la cardioïde (en rad).
const CARDIOID_ITERATION_NUMBER = 100;	// Nombre d'itéartions (= nombre de cercles constituant la cardioïde).

/* --- */

/*
 * Fonction de setup de p5.js.
 */
function setup() {
	createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);	// Création canvas.
	noFill();	// Remplissage vide.
	stroke(0);	// Contour noir.
	var angle = CARDIOID_ORIENTATION;	// Initialisation angle.
	var initialCoords = calculateCirclePointCoords(
		CARDIOID_INITIAL_CIRCLE_CENTER_X,
		CARDIOID_INITIAL_CIRCLE_CENTER_Y,
		CARDIOID_INITIAL_CIRCLE_RADIUS,
		angle
	);	// Coordonnées initiales (point appartenant à l'ensemble des cercles de la cardioïde.
	for (var i = 1; i <= CARDIOID_ITERATION_NUMBER; i++) {
		angle += 2 * Math.PI / CARDIOID_ITERATION_NUMBER;	// On ajoute une fraction d'angle.
		var center = calculateCirclePointCoords(
			CARDIOID_INITIAL_CIRCLE_CENTER_X,
			CARDIOID_INITIAL_CIRCLE_CENTER_Y,
			CARDIOID_INITIAL_CIRCLE_RADIUS,
			angle
		);	// On définit le centre du nouveau cercle.
		ellipse(
			center[0], 
			center[1], 
			Math.sqrt(Math.pow(center[0]- initialCoords[0], 2) + Math.pow(center[1] - initialCoords[1], 2)) * 2	// Longueur entre le point initial et le centre.
		);	// On le trace.
	}
}

/* 
 * Retourne les coordonnées d'un point M sur un cercle de rayon donné et de centre donné, à l'angle donné.
 * Paramètres :
 *  - xCenter : abscisse du centre du cercle (en px).
 *  - yCenter : ordonnée du centre du cercle (en px).
 *  - radius : rayon du cercle (en px).
 *  - angle : angle du point sur le cercle (en rad).
 * Retourne une array contenant les coordonnées du point.
 */
function calculateCirclePointCoords(xCenter, yCenter, radius, angle) {
	return [
		radius * Math.cos(angle) + xCenter, 
		radius * Math.sin(angle) + yCenter
	];
}